This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

`react` + `mobx` (model-views updates) + `antd` (UI components)

## How to start

`npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

##### Settings

Update constants in `./src/models/Utility.tsx` to change default behaviour.

## UI (short overview)

* Create one or several tasks:

![Create task](docs/0_task_create.png)

* To start work on selected task press `play` button:

![Current task in progress](docs/1_task_in_progress.png)

Other tasks would be unavailable to start during this period
 
* After maximum period of Pomodoro task (`45 minutes`) system will be force you to take short break (`5 minutes`) 
(This time does not include in task's spent time). After break working on task will be continued automatically.

![Short break](docs/2_task_on_break.png)
 
* After several work-short break periods (`4` by default) system will be force you to take long break (`15 minutes`).
(This time does not include in task's spent time). After this type of break working on task will **NOT** be continued.

![Long break](docs/3_task_on_long_break.png)

* You can also mark tasks as completed, delete outdated task, etc. 

![Current task in progress](docs/4_task_list_modify.png)

To keep person focused system does not allow have more than `15` uncompleted tasks in list.

