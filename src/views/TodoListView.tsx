import {inject, observer} from "mobx-react";
import React from "react";
import {Avatar, Badge, Button, Col, Icon, Row, Spin} from "antd";

import {TodoList} from "../models/Model";
import {TodoTask} from "../models/TodoTask";
import {MAX_UNCOMPLETED_TASKS} from "../models/Utility";

export interface TodoListProps     { todoStore?: TodoList }
export interface TodoListItemProps { todoStore?: TodoList, todo: TodoTask }

/** row in table list for task */
const TodoListItem = inject('todoStore')(
    observer(({ todoStore, todo }: TodoListItemProps) => {
        return <Row key={todo.id} className={todoStore!.isCurrent(todo) ? "Task-current" : ""}>
            <Col span={2}>
                <Badge count={todo.pomodoroCount}>
                    {(todo.isCompleted()) ? (
                        <Avatar style={{backgroundColor: '#87d068'}}><Icon type="smile-o"/></Avatar>
                    ) : (
                        <Avatar style={todo.isInProgress() ? {backgroundColor: "lightblue"} : {}}>
                            <Icon type={todo.isInProgress() ? "loading" : "border"}/>
                        </Avatar>
                    )}
                </Badge>
            </Col>
            <Col span={12}>
                <div className={"Task-title " + (todo.isCompleted() && "Task-completed")}>
                    <span>{todo.title}</span> <small>{todo.startedHumanTime()}</small>
                </div>
            </Col>
            <Col span={6}>
                <div className="Task-duration">{todo.spentHumanTime} &nbsp;
                    {todo.currentPomodoros.map(i => <Icon className="pomodoro" type="radius-bottomleft" />) }
                </div>
            </Col>
            <Col span={4}>
                <Button icon="check" disabled={todo.isCompleted()} onClick={() => todoStore!.done(todo)}/>
                <Button icon={todo.isInProgress() ? "pause" : "caret-right"}
                        disabled={!todoStore!.isAvailable(todo)} onClick={() => todoStore!.toggle(todo)}/>
                <Button type="danger" icon="delete" onClick={() => todoStore!.delete(todo)}/>
            </Col>
        </Row>
    }));

/** all tasks list */
export const TodoListView = inject('todoStore')(
    observer(({todoStore}: TodoListProps) => {
        return (<div className="Task-list">
            <Spin spinning={todoStore!.onBreak} tip="Take a break">
            {todoStore!.list.length > 0 && (
                <Row className="Task-header">
                    <Col span={2}>&nbsp;</Col>
                    <Col span={12}>Task</Col>
                    <Col span={6}>Time spent</Col>
                    <Col span={4}>Action</Col>
                </Row>
            )}
            {todoStore!.list.map(item => <TodoListItem key={item.id} todoStore={todoStore} todo={item}/>)}

            {todoStore!.isTooMuch && (
            <div className="Task-list-footer">You could not create more than {MAX_UNCOMPLETED_TASKS} uncompleted tasks</div>
            )}

            </Spin>
        </div>);
    })
);