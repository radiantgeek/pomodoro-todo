import {inject, observer} from "mobx-react";
import {observable} from "mobx";
import {Icon, Input, Steps} from "antd";
import React, {Component} from "react";

import {TodoListProps} from "./TodoListView";
import {POMODORO_BREAK, POMODORO_LONG_BREAK, POMODORO_WORK} from "../models/Utility";

const { Search } = Input;
const { Step } = Steps;

/** Countdown timer in page's right top - for current task or break */
export const TodoTimer = inject('todoStore')(
    observer(({todoStore}: TodoListProps) => {
        return (
            <div className={todoStore!.pom.currentStep > 1 ? "timer timer-break" : "timer"}>
                {todoStore!.pom.showTimer && <Icon type={todoStore!.pom.currentStep > 1 ? "coffee" : "loading"}/>}
                &nbsp; {todoStore!.pom.humanTimer}
            </div>
        );
    })
);

/** interactive short overview of pomodoro technique */
export const TodoHelp = inject('todoStore')(
    observer(({todoStore}: TodoListProps) => {
        return (
            <Steps progressDot current={todoStore!.helpStep}>
                <Step title="Start task"/>
                <Step title="Work&nbsp;with&nbsp;no&nbsp;distraction" description={"at most "+POMODORO_WORK+" minutes"}/>
                <Step title="Short break" description={"for " + POMODORO_BREAK + " minutes"}/>
                <Step title="Longer break" description={"at least " + POMODORO_LONG_BREAK + " minutes"}/>
                <Step title="Done"/>
            </Steps>
        );
    })
);

@inject('todoStore')
@observer
/** simple input to add new task to list */
export class NewTodoTaskForm extends Component<TodoListProps> {
    @observable private task: string = '';

    canAdd = () => {
        return !this.props.todoStore!.isTooMuch;
    }

    handleTaskChange = ({currentTarget: {value}}: React.SyntheticEvent<HTMLInputElement>) => {
        this.task = value;
    }

    handleAddTodo = () => {
        if (this.task.length > 0) {
            this.props.todoStore!.add(this.task);
            this.task = '';
        }
    }

    render() {
        return (<div>
            <Search
                prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                enterButton="Add"
                disabled={!this.canAdd()}
                onSearch={this.handleAddTodo}
                placeholder={!this.canAdd() ? "Too much uncompleted ToDo tasks" : "Enter new ToDo task"}
                value={this.task} onChange={this.handleTaskChange}/>
        </div>)
    }
}