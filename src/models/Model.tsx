import {action, computed, observable} from 'mobx';
import moment from "moment";

import {TodoTask} from "./TodoTask";
import {
    humanDuration,
    MAX_UNCOMPLETED_TASKS,
    POMODORO_BREAK,
    POMODORO_COUNT,
    POMODORO_LONG_BREAK,
    POMODORO_WORK
} from "./Utility";

/** pomodoro specific data for task list */
class PomodoroCurrent {
    /** current task */
    @observable current?: TodoTask;
    /** current step for pomodoro-helper header */
    @observable currentStep = 0;
    /** timeout deadline (for countdown timer) of current task or break / */
    @observable timeout?: Date;

    /** internal: recalculating model every second by timer */
    @observable lastCalculated: Date = new Date(); // for timer

    isCurrent(todo: TodoTask) {
        return this.current === todo;
    }

    /** check if 'start/pause' btn is available for task */
    isAvailable(todo: TodoTask) {
        return (this.current === undefined || this.current === todo && this.currentStep < 2) && todo.isAvailable();
    }

    @computed
    /** countdown timer in 'hh:mm' format */
    get humanTimer() {
        if (!this.showTimer) return "";
        let t = moment(this.timeout).diff(moment(this.lastCalculated));
        return humanDuration(moment.duration(t));
    }
    @computed
    /** show countdown timer only if there is current task or break on it */
    get showTimer() {
        return this.timeout !== undefined;
    }

    @action
    startWorkAt(todo: TodoTask) {
        this.currentStep = 1;
        this.current = todo;
        this.timeout = moment(moment.now()).add(POMODORO_WORK, "minutes").toDate();
        this.timerTick();
    }
    @action
    stopWorkAt() {
        this.currentStep = 0;
        this.current = undefined;
        this.timeout = undefined;
        this.timerTick();
    }
    @action
    /** internal: recalculating model every second by timer */
    timerTick()    {
        this.lastCalculated = new Date();
        if (this.timeout !== undefined && this.lastCalculated > this.timeout) { // reach timeout
            if (this.currentStep === 1) { // were working on task
                this.current!.stop();
                if (this.current!.pomodoroCount % POMODORO_COUNT === 0) {
                    this.currentStep = 3; // go on long break;
                    this.timeout = moment(moment.now()).add(POMODORO_LONG_BREAK, "minutes").toDate();
                } else {
                    this.currentStep = 2; // go on break;
                    this.timeout = moment(moment.now()).add(POMODORO_BREAK, "minutes").toDate();
                }
                return;
            }
            if (this.currentStep === 2) { // was on short break
                this.current!.start();
                this.currentStep = 1; // resume work;
                this.timeout = moment(moment.now()).add(POMODORO_WORK, "minutes").toDate();
            }
            if (this.currentStep === 3) { // were on long break
                this.stopWorkAt();
                this.currentStep = 0;
            }
        }
    }
}

/** model for task list */
export class TodoList {
    @observable list: TodoTask[] = [];
    @observable pom = new PomodoroCurrent();

    constructor(){
        setInterval(() => this.timerTick(), 1000);
    }
    isCurrent(todo: TodoTask) {
        return this.pom.isCurrent(todo);
    }
    isAvailable(todo: TodoTask) {
        return this.pom.isAvailable(todo);
    }

    @computed
    get onBreak() {
        return this.pom.currentStep > 1;
    }
    @computed
    get helpStep() {
        return this.pom.currentStep;
    }
    @computed
    get completed(): number {
        return this.list.filter((todo) => todo.isCompleted()).length
    }
    @computed
    get isTooMuch(): boolean {
        return this.list.filter((todo) => !todo.isCompleted()).length >= MAX_UNCOMPLETED_TASKS
    }

    @action
    add(title: string) {
        this.list.push(new TodoTask(title))
    }
    @action
    delete(t: TodoTask) {
        this.pom.stopWorkAt();
        let i = this.list.indexOf(t);
        if (i > -1) {
            this.list.splice(i, 1);
        }
    }
    @action
    /** toggle current task state by 'start/pause' btn */
    toggle(item: TodoTask) {
        let t = this.list.find((todo) => todo === item);
        if (t !== undefined) {
            if (t.isInProgress()) {
                this.pom.stopWorkAt();
                t.stop()
            } else {
                this.pom.startWorkAt(t);
                t.start();
            }
        }
    }
    @action
    done(item: TodoTask) {
        this.pom.stopWorkAt();
        let t = this.list.find((todo) => todo === item);
        if (t !== undefined) {
            if (t.isInProgress()) t.stop();
            t.complete();
        }
    }

    @action
    /** internal: recalculating model every second by timer */
    timerTick() { this.pom.timerTick(); this.list.forEach( todo => todo.timerTick() ) }
}
