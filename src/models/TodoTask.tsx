import {action, computed, observable} from "mobx";
import moment from "moment";
import {humanDuration, POMODORO_COUNT, TIMEOUT, uniqId} from "./Utility";

/** basic class of pomodoro-specified */
export class PomodoroTask {
    /** current count of pomodoro spent on task*/
    @observable pomodoroCount: number = 0;

    /** sum of durations for every already finished pomodoro spent on task */
    @observable duration = moment.duration(0);
    /** for current task: time of resume, null otherwise */
    @observable resumedAt?: Date = undefined;
    /** only for previously paused task */
    @observable pausedAt?: Date = undefined;

    /** internal: recalculating model every second by timer */
    @observable lastCalculated: Date = new Date();

    @computed
    /** total spent time (including current pomodoro) */
    get spentHumanTime() {
        let t = moment.duration(this.duration).add(this.currentDuration());
        return humanDuration(t);
    }
    @computed
    /** count of pomodoro of short breaks (from 0 till 3) */
    get currentPomodoros() {
        let r = [];
        let c = (this.pomodoroCount-1) % POMODORO_COUNT + 1;
        for (let i=0; i < c; i++) r.push(i);
        return r;
    }

    /** for current task: time duration spent in current pomodoro */
    currentDuration() {
        if (this.resumedAt !== undefined)
            return moment.duration(Math.abs(moment(this.lastCalculated).diff(this.resumedAt)));
        else
            moment.duration(0);
    }

    /** check is it current task */
    isInProgress() {
        return this.resumedAt != undefined;
    }

    /** check if 'start/pause' btn is available */
    isAvailable() {
        let t = new Date(this.lastCalculated);
        t.setSeconds(t.getSeconds() - TIMEOUT);
        return this.pausedAt === undefined || this.pausedAt < t;
    }

    @action
    /** start/resume working on task */
    start() {
        this.pomodoroCount++;
        this.resumedAt = new Date();
        this.lastCalculated = this.resumedAt;
        this.pausedAt = undefined;
    }

    @action
    /** pause current task */
    stop() {
        this.duration = this.duration.add(this.currentDuration());
        this.resumedAt = undefined;
        this.pausedAt = new Date();
    }

    @action
    /** internal: recalculating model every second by timer */
    timerTick() {
        this.lastCalculated = new Date();
    }
}

/** extends base model with business data */
export class TodoTask extends PomodoroTask {
    id: string;
    @observable title: string;

    /** first time to work on task*/
    @observable startedAt: Date = new Date();

    /** task finish (no more work on it) */
    @observable completed: boolean = false;
    @observable completedAt?: Date = undefined;

    constructor(title: string) {
        super();
        this.id = uniqId();
        this.title = title;
    }

   /** check if 'start/pause' btn is available */
    isAvailable() {
        return !this.isCompleted() && super.isAvailable();
    }

    /** task creating time in human format (like "2 minutes ago")*/
    startedHumanTime() {
        return this.startedAt !== undefined ? moment(this.startedAt).fromNow() : "";
    }

    isCompleted() {
        return this.completed;
    }

    @action
    /** mark task as completed, no more work on it */
    complete() {
        this.completed = true;
        this.resumedAt = undefined;
        this.pausedAt = undefined;
    }
}