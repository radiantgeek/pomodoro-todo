import moment from "moment";

/** maximum count of uncompleted tasks -- disable creating new before this limits */
export const MAX_UNCOMPLETED_TASKS = 15;

/** timeout between new restart working on task (in seconds) */
export const TIMEOUT = 1;

/** maximum time to work on task before break (size of pomodoro, in minutes) */
export const POMODORO_WORK = 45;

/** minimum time to spent on short break before restart working on task (in minutes) */
export const POMODORO_BREAK = 5;

/** count of pomodoros before long break */
export const POMODORO_COUNT = 4;

/** minimum time to spent on long break (after 4th pomodoro) before restart working on task (in minutes) */
export const POMODORO_LONG_BREAK = 15;

/** generate unique id as string (task id) */
export function uniqId()      {
    let a = "0"; for (let i=1; i<10; i++) { a += Math.random()*10|0; }
    return a;
}

/** print duration in 'HH:mm' format */
export function humanDuration(duration: moment.Duration) {
    if (duration.asSeconds() < 0) return "0:00";
    let s = duration.seconds();
    return duration.minutes() + ":" + Math.floor(s/10) + "" + (s%10);
}
