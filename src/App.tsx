import {Provider} from 'mobx-react';
import React, {Component} from 'react';

import {TodoList} from './models/Model';
import {TodoListView} from "./views/TodoListView";
import {NewTodoTaskForm, TodoHelp, TodoTimer} from "./views/HeaderView";

import './App.css';

export class App extends Component {
    private todoStore: TodoList = new TodoList()

    render() {
        return (
            <Provider todoStore={this.todoStore}>
                <div className="App">
                    <TodoTimer/>
                    <header className="App-header">
                        <h3>Todo with Pomodoro</h3>
                    </header>
                    <TodoHelp/>
                    <NewTodoTaskForm/>
                    <TodoListView />
                </div>
            </Provider>
        );
    }
}

export default App;
